package com.ruoyi.project.nczxwechat.stuinfo.mapper;

import java.util.List;
import com.ruoyi.project.nczxwechat.stuinfo.domain.WxStuinfo;

/**
 * 学生信息Mapper接口
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-16
 */
public interface WxStuinfoMapper 
{
    /**
     * 查询学生信息
     * 
     * @param id 学生信息主键
     * @return 学生信息
     */
    public WxStuinfo selectWxStuinfoById(Long id);

    /**
     * 查询学生信息列表
     * 
     * @param wxStuinfo 学生信息
     * @return 学生信息集合
     */
    public List<WxStuinfo> selectWxStuinfoList(WxStuinfo wxStuinfo);

    /**
     * 新增学生信息
     * 
     * @param wxStuinfo 学生信息
     * @return 结果
     */
    public int insertWxStuinfo(WxStuinfo wxStuinfo);

    /**
     * 修改学生信息
     * 
     * @param wxStuinfo 学生信息
     * @return 结果
     */
    public int updateWxStuinfo(WxStuinfo wxStuinfo);

    /**
     * 删除学生信息
     * 
     * @param id 学生信息主键
     * @return 结果
     */
    public int deleteWxStuinfoById(Long id);

    /**
     * 批量删除学生信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWxStuinfoByIds(String[] ids);

    /**
     * 通过学生唯一ID查询学生信息
     * @param stuno 学生唯一ID
     * @return
     */
    public WxStuinfo selectWxStuinfoByStunNo(String stuno);

    /**
     * 通过学生唯一ID与微信openid查询学生信息
     * @param stuno
     * @param openId
     * @return
     */
    public WxStuinfo selectWxStuinfoByStunNoAndOpenID(String stuno, String openId);
    /**
     * 通过学生ID来更新学生选课状态-选课
     * @param stuno
     * @return
     */
    public int updateWxStuinfoSelectStateByStuNo(String stuno);
    /**
     * 通过学生ID来更新学生选课状态-未选课
     * @param stuno
     * @return
     */
    public int updateWxStuinfoSelectStateByStuNo1(String stuno);
}
