package com.ruoyi.project.nczxwechat.stuinfo.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.nczxwechat.stuinfo.mapper.WxStuinfoMapper;
import com.ruoyi.project.nczxwechat.stuinfo.domain.WxStuinfo;
import com.ruoyi.project.nczxwechat.stuinfo.service.IWxStuinfoService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 学生信息Service业务层处理
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-16
 */
@Service
public class WxStuinfoServiceImpl implements IWxStuinfoService 
{
    @Autowired
    private WxStuinfoMapper wxStuinfoMapper;

    /**
     * 查询学生信息
     * 
     * @param id 学生信息主键
     * @return 学生信息
     */
    @Override
    public WxStuinfo selectWxStuinfoById(Long id)
    {
        return wxStuinfoMapper.selectWxStuinfoById(id);
    }

    /**
     * 查询学生信息列表
     * 
     * @param wxStuinfo 学生信息
     * @return 学生信息
     */
    @Override
    public List<WxStuinfo> selectWxStuinfoList(WxStuinfo wxStuinfo)
    {
        return wxStuinfoMapper.selectWxStuinfoList(wxStuinfo);
    }

    /**
     * 新增学生信息
     * 
     * @param wxStuinfo 学生信息
     * @return 结果
     */
    @Override
    public int insertWxStuinfo(WxStuinfo wxStuinfo)
    {
        return wxStuinfoMapper.insertWxStuinfo(wxStuinfo);
    }

    /**
     * 修改学生信息
     * 
     * @param wxStuinfo 学生信息
     * @return 结果
     */
    @Override
    public int updateWxStuinfo(WxStuinfo wxStuinfo)
    {
        return wxStuinfoMapper.updateWxStuinfo(wxStuinfo);
    }

    /**
     * 批量删除学生信息
     * 
     * @param ids 需要删除的学生信息主键
     * @return 结果
     */
    @Override
    public int deleteWxStuinfoByIds(String ids)
    {
        return wxStuinfoMapper.deleteWxStuinfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除学生信息信息
     * 
     * @param id 学生信息主键
     * @return 结果
     */
    @Override
    public int deleteWxStuinfoById(Long id)
    {
        return wxStuinfoMapper.deleteWxStuinfoById(id);
    }

    /**
     * 通过学生唯一ID查询学生信息
     * @param stuno 学生唯一ID
     * @return
     */
    @Override
    public WxStuinfo selectWxStuinfoByStuNo(String stuno) {
        return  wxStuinfoMapper.selectWxStuinfoByStunNo(stuno);
    }

    /**
     * 通过学生唯一ID与微信openid查询学生信息
     * @param stuno
     * @param openId
     * @return
     */
    @Override
    public WxStuinfo selectWxStuinfoByStuNoAndOpenID(String stuno, String openId) {
        return wxStuinfoMapper.selectWxStuinfoByStunNoAndOpenID(stuno,openId);
    }
    /**
     * 通过学生ID来更新学生选课状态-选课
     * @param stuno
     * @return
     */
    @Override
    public int updateWxStuinfoSelectStateByStuNo(String stuno) {
        return wxStuinfoMapper.updateWxStuinfoSelectStateByStuNo(stuno);
    }
    /**
     * 通过学生ID来更新学生选课状态-未选课
     * @param stuno
     * @return
     */
    @Override
    public int updateWxStuinfoSelectStateByStuNo1(String stuno) {
        return wxStuinfoMapper.updateWxStuinfoSelectStateByStuNo1(stuno);
    }
}
