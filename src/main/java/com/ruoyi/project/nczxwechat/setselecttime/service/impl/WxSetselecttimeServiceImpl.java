package com.ruoyi.project.nczxwechat.setselecttime.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.nczxwechat.setselecttime.mapper.WxSetselecttimeMapper;
import com.ruoyi.project.nczxwechat.setselecttime.domain.WxSetselecttime;
import com.ruoyi.project.nczxwechat.setselecttime.service.IWxSetselecttimeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 选课时间设定Service业务层处理
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-21
 */
@Service
public class WxSetselecttimeServiceImpl implements IWxSetselecttimeService 
{
    @Autowired
    private WxSetselecttimeMapper wxSetselecttimeMapper;

    /**
     * 查询选课时间设定
     * 
     * @param id 选课时间设定主键
     * @return 选课时间设定
     */
    @Override
    public WxSetselecttime selectWxSetselecttimeById(Long id)
    {
        return wxSetselecttimeMapper.selectWxSetselecttimeById(id);
    }

    /**
     * 查询选课时间设定列表
     * 
     * @param wxSetselecttime 选课时间设定
     * @return 选课时间设定
     */
    @Override
    public List<WxSetselecttime> selectWxSetselecttimeList(WxSetselecttime wxSetselecttime)
    {
        return wxSetselecttimeMapper.selectWxSetselecttimeList(wxSetselecttime);
    }

    /**
     * 新增选课时间设定
     * 
     * @param wxSetselecttime 选课时间设定
     * @return 结果
     */
    @Override
    public int insertWxSetselecttime(WxSetselecttime wxSetselecttime)
    {
        wxSetselecttime.setCreateTime(DateUtils.getNowDate());
        return wxSetselecttimeMapper.insertWxSetselecttime(wxSetselecttime);
    }

    /**
     * 修改选课时间设定
     * 
     * @param wxSetselecttime 选课时间设定
     * @return 结果
     */
    @Override
    public int updateWxSetselecttime(WxSetselecttime wxSetselecttime)
    {
        wxSetselecttime.setUpdateTime(DateUtils.getNowDate());
        return wxSetselecttimeMapper.updateWxSetselecttime(wxSetselecttime);
    }

    /**
     * 批量删除选课时间设定
     * 
     * @param ids 需要删除的选课时间设定主键
     * @return 结果
     */
    @Override
    public int deleteWxSetselecttimeByIds(String ids)
    {
        return wxSetselecttimeMapper.deleteWxSetselecttimeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除选课时间设定信息
     * 
     * @param id 选课时间设定主键
     * @return 结果
     */
    @Override
    public int deleteWxSetselecttimeById(Long id)
    {
        return wxSetselecttimeMapper.deleteWxSetselecttimeById(id);
    }
    /**
     * 通过学校ID查询当前学校选课设定时间
     * @param ognid
     * @return
     */
    @Override
    public WxSetselecttime selectWxSetselecttimeBySchoolId(String ognid) {
        return wxSetselecttimeMapper.selectWxSetselecttimeBySchoolId(ognid);
    }
}
