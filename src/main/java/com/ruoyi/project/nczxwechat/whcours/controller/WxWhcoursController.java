package com.ruoyi.project.nczxwechat.whcours.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.nczxwechat.whcours.domain.WxWhcours;
import com.ruoyi.project.nczxwechat.whcours.service.IWxWhcoursService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 文化课程信息Controller
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-20
 */
@Controller
@RequestMapping("/nczxwechat/whcours")
public class WxWhcoursController extends BaseController
{
    private String prefix = "nczxwechat/whcours";

    @Autowired
    private IWxWhcoursService wxWhcoursService;

    @RequiresPermissions("nczxwechat:whcours:view")
    @GetMapping()
    public String whcours()
    {
        return prefix + "/whcours";
    }

    /**
     * 查询文化课程信息列表
     */
    @RequiresPermissions("nczxwechat:whcours:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WxWhcours wxWhcours)
    {
        startPage();
        List<WxWhcours> list = wxWhcoursService.selectWxWhcoursList(wxWhcours);
        return getDataTable(list);
    }

    /**
     * 导出文化课程信息列表
     */
    @RequiresPermissions("nczxwechat:whcours:export")
    @Log(title = "文化课程信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WxWhcours wxWhcours)
    {
        List<WxWhcours> list = wxWhcoursService.selectWxWhcoursList(wxWhcours);
        ExcelUtil<WxWhcours> util = new ExcelUtil<WxWhcours>(WxWhcours.class);
        return util.exportExcel(list, "文化课程信息数据");
    }

    /**
     * 新增文化课程信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存文化课程信息
     */
    @RequiresPermissions("nczxwechat:whcours:add")
    @Log(title = "文化课程信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WxWhcours wxWhcours)
    {
        return toAjax(wxWhcoursService.insertWxWhcours(wxWhcours));
    }

    /**
     * 修改文化课程信息
     */
    @RequiresPermissions("nczxwechat:whcours:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WxWhcours wxWhcours = wxWhcoursService.selectWxWhcoursById(id);
        mmap.put("wxWhcours", wxWhcours);
        return prefix + "/edit";
    }

    /**
     * 修改保存文化课程信息
     */
    @RequiresPermissions("nczxwechat:whcours:edit")
    @Log(title = "文化课程信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WxWhcours wxWhcours)
    {
        return toAjax(wxWhcoursService.updateWxWhcours(wxWhcours));
    }

    /**
     * 删除文化课程信息
     */
    @RequiresPermissions("nczxwechat:whcours:remove")
    @Log(title = "文化课程信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wxWhcoursService.deleteWxWhcoursByIds(ids));
    }
}
