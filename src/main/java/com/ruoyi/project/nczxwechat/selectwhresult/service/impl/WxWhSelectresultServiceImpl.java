package com.ruoyi.project.nczxwechat.selectwhresult.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.nczxwechat.selectwhresult.mapper.WxWhSelectresultMapper;
import com.ruoyi.project.nczxwechat.selectwhresult.domain.WxWhSelectresult;
import com.ruoyi.project.nczxwechat.selectwhresult.service.IWxWhSelectresultService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 文化课程Service业务层处理
 * 
 * @author 阿卜QQ932696181
 * @date 2022-01-19
 */
@Service
public class WxWhSelectresultServiceImpl implements IWxWhSelectresultService 
{
    @Autowired
    private WxWhSelectresultMapper wxWhSelectresultMapper;

    /**
     * 查询文化课程
     * 
     * @param id 文化课程主键
     * @return 文化课程
     */
    @Override
    public WxWhSelectresult selectWxWhSelectresultById(Long id)
    {
        return wxWhSelectresultMapper.selectWxWhSelectresultById(id);
    }

    /**
     * 查询文化课程列表
     * 
     * @param wxWhSelectresult 文化课程
     * @return 文化课程
     */
    @Override
    public List<WxWhSelectresult> selectWxWhSelectresultList(WxWhSelectresult wxWhSelectresult)
    {
        return wxWhSelectresultMapper.selectWxWhSelectresultList(wxWhSelectresult);
    }

    /**
     * 新增文化课程
     * 
     * @param wxWhSelectresult 文化课程
     * @return 结果
     */
    @Override
    public int insertWxWhSelectresult(WxWhSelectresult wxWhSelectresult)
    {
        wxWhSelectresult.setCreateTime(DateUtils.getNowDate());
        return wxWhSelectresultMapper.insertWxWhSelectresult(wxWhSelectresult);
    }

    /**
     * 修改文化课程
     * 
     * @param wxWhSelectresult 文化课程
     * @return 结果
     */
    @Override
    public int updateWxWhSelectresult(WxWhSelectresult wxWhSelectresult)
    {
        wxWhSelectresult.setUpdateTime(DateUtils.getNowDate());
        return wxWhSelectresultMapper.updateWxWhSelectresult(wxWhSelectresult);
    }

    /**
     * 批量删除文化课程
     * 
     * @param ids 需要删除的文化课程主键
     * @return 结果
     */
    @Override
    public int deleteWxWhSelectresultByIds(String ids)
    {
        return wxWhSelectresultMapper.deleteWxWhSelectresultByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除文化课程信息
     * 
     * @param id 文化课程主键
     * @return 结果
     */
    @Override
    public int deleteWxWhSelectresultById(Long id)
    {
        return wxWhSelectresultMapper.deleteWxWhSelectresultById(id);
    }

    /**
     * 通过学生唯一编号查询选课信息
     * @param stuno
     * @return
     */
    @Override
    public WxWhSelectresult selectWxWhSelectresultByStuno(String stuno) {
        return wxWhSelectresultMapper.selectWxWhSelectresultByStuno(stuno);
    }
    /**
     * 通过学生唯一ID删除已选的课程信息
     * @param stuno
     * @return
     */
    @Override
    public int deleteWxWhSelectresultByStuno(String stuno) {
        return wxWhSelectresultMapper.deleteWxWhSelectresultByStuno(stuno);
    }
}
