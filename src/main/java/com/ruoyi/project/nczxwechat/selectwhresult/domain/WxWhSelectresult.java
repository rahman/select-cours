package com.ruoyi.project.nczxwechat.selectwhresult.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 文化课程对象 nczx_wx_selectresult
 * 
 * @author 阿卜QQ932696181
 * @date 2022-01-19
 */
public class WxWhSelectresult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    private Long id;

    /** 学生ID */
    @Excel(name = "学生ID")
    private String stuno;

    /** 学校代码 */
    private String schooid;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolname;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String stuname;

    /** 性别 */
    private String stusex;

    /** 学生班级 */
    @Excel(name = "学生班级")
    private String stuclass;

    /** 选择的课程 */
    @Excel(name = "选择的课程")
    private String selectinfo;

    /** 用户openId */
    private String openId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStuno(String stuno)
    {
        this.stuno = stuno;
    }

    public String getStuno()
    {
        return stuno;
    }
    public void setSchooid(String schooid)
    {
        this.schooid = schooid;
    }

    public String getSchooid()
    {
        return schooid;
    }
    public void setSchoolname(String schoolname)
    {
        this.schoolname = schoolname;
    }

    public String getSchoolname()
    {
        return schoolname;
    }
    public void setStuname(String stuname)
    {
        this.stuname = stuname;
    }

    public String getStuname()
    {
        return stuname;
    }
    public void setStusex(String stusex)
    {
        this.stusex = stusex;
    }

    public String getStusex()
    {
        return stusex;
    }
    public void setStuclass(String stuclass)
    {
        this.stuclass = stuclass;
    }

    public String getStuclass()
    {
        return stuclass;
    }
    public void setSelectinfo(String selectinfo)
    {
        this.selectinfo = selectinfo;
    }

    public String getSelectinfo()
    {
        return selectinfo;
    }
    public void setOpenId(String openId)
    {
        this.openId = openId;
    }

    public String getOpenId()
    {
        return openId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("stuno", getStuno())
            .append("schooid", getSchooid())
            .append("schoolname", getSchoolname())
            .append("stuname", getStuname())
            .append("stusex", getStusex())
            .append("stuclass", getStuclass())
            .append("selectinfo", getSelectinfo())
            .append("openId", getOpenId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
