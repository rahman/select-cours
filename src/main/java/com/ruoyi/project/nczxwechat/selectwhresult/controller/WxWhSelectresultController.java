package com.ruoyi.project.nczxwechat.selectwhresult.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.nczxwechat.selectwhresult.domain.WxWhSelectresult;
import com.ruoyi.project.nczxwechat.selectwhresult.service.IWxWhSelectresultService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 文化课程Controller
 * 
 * @author 阿卜QQ932696181
 * @date 2022-01-19
 */
@Controller
@RequestMapping("/nczxwechat/selectwhresult")
public class WxWhSelectresultController extends BaseController
{
    private String prefix = "nczxwechat/selectwhresult";

    @Autowired
    private IWxWhSelectresultService wxWhSelectresultService;

    @RequiresPermissions("nczxwechat:selectwhresult:view")
    @GetMapping()
    public String selectwhresult()
    {
        return prefix + "/selectwhresult";
    }

    /**
     * 查询文化课程列表
     */
    @RequiresPermissions("nczxwechat:selectwhresult:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WxWhSelectresult wxWhSelectresult)
    {
        startPage();
        List<WxWhSelectresult> list = wxWhSelectresultService.selectWxWhSelectresultList(wxWhSelectresult);
        return getDataTable(list);
    }

    /**
     * 导出文化课程列表
     */
    @RequiresPermissions("nczxwechat:selectwhresult:export")
    @Log(title = "文化课程", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WxWhSelectresult wxWhSelectresult)
    {
        List<WxWhSelectresult> list = wxWhSelectresultService.selectWxWhSelectresultList(wxWhSelectresult);
        ExcelUtil<WxWhSelectresult> util = new ExcelUtil<WxWhSelectresult>(WxWhSelectresult.class);
        return util.exportExcel(list, "文化课程数据");
    }

    /**
     * 新增文化课程
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存文化课程
     */
    @RequiresPermissions("nczxwechat:selectwhresult:add")
    @Log(title = "文化课程", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WxWhSelectresult wxWhSelectresult)
    {
        return toAjax(wxWhSelectresultService.insertWxWhSelectresult(wxWhSelectresult));
    }

    /**
     * 修改文化课程
     */
    @RequiresPermissions("nczxwechat:selectwhresult:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WxWhSelectresult wxWhSelectresult = wxWhSelectresultService.selectWxWhSelectresultById(id);
        mmap.put("wxWhSelectresult", wxWhSelectresult);
        return prefix + "/edit";
    }

    /**
     * 修改保存文化课程
     */
    @RequiresPermissions("nczxwechat:selectwhresult:edit")
    @Log(title = "文化课程", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WxWhSelectresult wxWhSelectresult)
    {
        return toAjax(wxWhSelectresultService.updateWxWhSelectresult(wxWhSelectresult));
    }

    /**
     * 删除文化课程
     */
    @RequiresPermissions("nczxwechat:selectwhresult:remove")
    @Log(title = "文化课程", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wxWhSelectresultService.deleteWxWhSelectresultByIds(ids));
    }
}
