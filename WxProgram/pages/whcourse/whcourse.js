import util from '../../utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        kxcourslist: [],
        checkNum: 0,
        max:false,
        maxCheckedNum: 3,
        selectdata:[],
    },
    checkChange(e) {
        console.log(e)
        let id = e.currentTarget.id;
        this.data.kxcourslist[id].checked = !this.data.kxcourslist[id].checked;
        this.data.checkNum = this.data.kxcourslist[id].checked ? this.data.checkNum + 1 : this.data.checkNum - 1;
        this.checkMax(this.data.checkNum);
        // console.log(this.data.kxcourslist)
    },
    checkMax(num) {
        const maxNum = this.data.maxCheckedNum;
        const kxcourslist = this.data.kxcourslist;
        if (num == maxNum) {
            var status = true;
        } else if (num < maxNum && this.data.max) {
            var status = false;
        }
        if (status != undefined) {
            this.data.max = status;
            for (var i = 0; i < kxcourslist.length; i++) {
                if (!kxcourslist[i].checked) kxcourslist[i].canCheck = status;
            }
            this.setData({
                kxcourslist: kxcourslist
            })
        }
    },
    overSelect:function(e){
          let checked = "";
          const len = 0;
          const items = this.data.kxcourslist;
          for (var i = 0; i < items.length; i++) {
                if(items[i].checked){
                    checked+=items[i].courseName+","
                }
            }
          console.log(checked)
      //物理,化学,生物,checked.substring(0,checked.length - 1)去除最后一个字符串
        const selectinfo = checked.substring(0,checked.length - 1);
        var userinfo = wx.getStorageSync('userInfo');
        let params = {
          stuno:userinfo.stuno,
          selectinfo:selectinfo
        }
        if(checked.length<3){
          wx.showToast({
            title: '至少选择三门课程！',
            icon:'error'
          })
        }else{
          util.post('selectCourse',params,response=>{
            const data = response
            if (data.code === 0) {
              console.log("----选课成功！----",data)
              wx.hideLoading() //关闭提示框
              wx.switchTab({
                url: '/pages/user/index/index', //跳转到指定页面
              })
            }else{
              wx.showToast({
                title: data.msg,
                icon: 'none',
                duration: 2000
              })
            }
         })
        }
          
      },

    onLoad: function (options) {

    },

   
    onShow: function () {
        this.whcourselist();
    },
//文化课程加载
whcourselist:function(){
    var userinfo = wx.getStorageSync('userInfo');
    util.get(`getWhCourseList?schoolid=${userinfo.schooid}`,response=>{
      console.log(response)
      if(response){
        if (response.code==0) {
          console.log("数据加载成功！");
          this.setData({
            kxcourslist:response.data,
          })
        }else{
          wx.showToast({
            title: '本校暂未设置选课！请耐心等待',
            icon:'none',
            duration:2000
          })
        }
        this.setData({
          isLoad:false
        })
      }
    })
  },
})