// pages/home/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const userinfo = wx.getStorageSync('userInfo');
    const token = wx.getStorageSync('token');
    console.log(userinfo)
    if (userinfo != null && userinfo != ''&&token!='') {
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  tostudent:function(){
    wx.navigateTo({
      url: '../login/index',
    })
  },
  toschool:function(){
    wx.showToast({
      title: '暂未开放',
      icon:'error'
    })
  }
})