import util from '../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courslist:[
      // {name:'《红楼梦》整本书阅读',teacher:'赵洪英',desc:'通过对《红楼梦》进行研究，感受章回体小说独特的美。',num:'5'},
      // {name:'思辨性阅读与表达学习',teacher:'于雯',desc:'本课程着眼于发展学生的实证、批判与发现能力，通过生本对话，发现文学作品的独特之美，碰撞思维火花，强化思辨意识，提升学生的思辨能力',num:'5'},
      // {name:'北运河畔的乡土往事',teacher:'王梦晴',desc:'根植于北运河的北辰文化不仅有着悠久的历史，还影响着当地人的生活习俗，凝聚着北辰精神。作为北辰地区的高中生，研究本土的文化能大大提高他们的学习兴趣，引发他们对于当代文化的和如何传承传统文化的思考。',num:'5'},
      // {name:'构建阅读写作一体化课程',teacher:'叶曙霞',desc:'由阅读引发写作，以写作促进阅读',num:'5'},
      // {name:'多元化思维与思辩语文',teacher:'刘桂云',desc:'本课程基于新课程标准要求培养学生思辨能力的要求，立足大语文课堂，以作文教学为切入点。针对社会热点新闻事实，展开教学，进行评论，开展辩论，培养学生独立思考的能力，正确评述新闻事实，多元化多角度看待问题，以养成正确的思辨习惯。',num:'5'},
    ],
    basicsList: [{
      icon: 'usefullfill',
      name: '阅读'
    }, {
      icon: 'roundcheckfill',
      name: '看剩余数'
    }, {
      icon: 'roundcheckfill',
      name: '点击选择'
    }, {
      icon: 'roundcheckfill',
      name: '确定选择'
    }, ],
    isLoad:true,
  },
  //选课列表
  courslist:function(id){
    util.get(`loadCoursList?kcflid=${id}`,response=>{
      console.log(response)
      if(response){
        if (response.code==0) {
          this.setData({
            courslist:response.data
          })
        }else{
          wx.showToast({
            title: response.msg,
            icon:'none',
            duration:5000
          })
        }
        this.setData({
          isLoad:false
        })
      }
    })
  },

  onLoad: function (options) {
      console.log(options)
      var that =this;
      that.setData({
        title:options.title,
        kcid:options.id
      })
      that.courslist(options.id);
  },
/**
 * 选课
 * @param {*} e 
 */
  selectCours(e) {
    console.log(e)
    var clickdta =e.currentTarget.dataset;
    this.setData({
      modalName: clickdta.target,
      teacher:clickdta.teacher,
      kc:clickdta.title,
      fid:clickdta.id
    })
  },
  confirmSelect(e) {
    console.log(e)
    var userinfo = wx.getStorageSync('userInfo');
    let params = {
      kcid:e.currentTarget.dataset.id,
      stuno:userinfo.stuno,
      stuname:userinfo.stuname,
      stuclass:userinfo.stuclass,
      openId:userinfo.openId,
      schooid:userinfo.schooid,
      kcfl:e.currentTarget.dataset.title
    }
    util.post('selectCours',params,response=>{
      // console.log("----请求成功----",response)
      if (response.code === 0) {
        wx.switchTab({
          url: '/pages/user/index/index', //跳转到指定页面
        })
      }else{
        wx.showToast({
          title: response.msg,
          icon: 'none',
          duration: 5000
        })
        this.courslist(e.currentTarget.dataset.kcid)
      }
   })
    this.hideModal()
  },
  hideModal() {
    this.setData({
      modalName: null
    })
  },
})