// pages/user/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
       const userinfo = wx.getStorageSync("userInfo");
       const token = wx.getStorageSync("token");
       console.log(userinfo)
       if(!token){
        wx.switchTab({
          url: '/pages/login/index',
        })
       }else{
        this.setData({
          userInfo:userinfo
        })
      }
  },
  onShow:function(){
    this.onLoad()
  },
  outLogin:function(e){
    var that = this;
    that.showModal(e)
  },
  showModal(e) {
    console.log(e)
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  confirim:function(e){
    wx.clearStorage({
      success: (res) => {
        this.hideModal();
        wx.navigateTo({
          url: '../../login/index',
        })
      },
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
})